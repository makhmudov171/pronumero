<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distance extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\Role');
    }
}
