<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Distance;

class DistanceController extends Controller
{
    public function index()
    {
        $data = Distance::all();
        return view('backend.distance.index',compact('data'));
    }
    public function create()
    {
        return view('backend.distance.create');
    }
    public function edit($id)
    {
        $data = Distance::findOrFail($id);
        return view('backend.distance.edit',compact('data'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'long' => 'required',
            'place' => 'required',
            'time' => 'required',
            'price' => 'required',
            'price_usd' => 'required',
        ]);
        Distance::create([
            'long'=> request('long'),
            'date'=> request('date'),
            'place'=> request('place'),
            'time'=> request('time'),
            'price'=> request('price'),
            'price_usd'=> request('price_usd'),
        ]);
        return redirect()->action('DistanceController@index')->with('success','Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        request()->validate([
            'long' => 'required',
            'place' => 'required',
            'time' => 'required',
            'price' => 'required',
            'price_usd' => 'required',
        ]);
        Distance::find($id)->update(request()->all());
        return redirect()->action('DistanceController@index')->with('success','Успешно обновлено');
    }
}
