<?php

namespace App\Http\Controllers;

use Auth;
use GuzzleHttp\Client;
use App\Distance;
use App\Resource;
use App\Pages;
class PageController extends Controller
{
    public function index()
    {
        $runners = [];
        return view('frontend.index',compact('runners'));
    }
    public function summary()
    {
        return view('frontend.summary');
    }
    public function program_first()
    {
        $lang = app()->getLocale();
        $data = Pages::where('lang',$lang)->where('name','program1')->first();
        return view('frontend.program',compact('data'));
    }
    public function program_second()
    {
        $lang = app()->getLocale();
        $data = Pages::where('lang',$lang)->where('name','program2')->first();
        return view('frontend.program',compact('data'));
    }
    public function program_third()
    {
        $lang = app()->getLocale();
        $data = Pages::where('lang',$lang)->where('name','program3')->first();
        return view('frontend.program',compact('data'));
    }
    public function program_fourth()
    {
        $lang = app()->getLocale();
        $data = Pages::where('lang',$lang)->where('name','program4')->first();
        return view('frontend.program',compact('data'));
    }
    public function information()
    {
        $lang = app()->getLocale();
        $data = Pages::where('lang',$lang)->first();
        return view('frontend.test',compact('data'));
    }
    public function register()
    {
        if(Auth::check()){
            if(Auth::user()->is_paid){
                return redirect()->action('PageController@success');
            }
            return redirect()->action('PageController@payment');
        }
        $data = Distance::all();
        return view('frontend.register',compact('data'));
    }
    public function payment()
    {
        if(Auth::check()) {
            if (Auth::user()->is_paid) {
                return redirect()->action('PageController@success');
            }
            $data = Auth::user()->distance()->first();
            return view('frontend.payment',compact('data'));
        }
        else{
            return redirect()->action('PageController@register');
        }
        }
    public function success()
    {
        $data = \Auth::user()->distance;
        return view('frontend.success',compact('data'));
    }
    public function about()
    {
        return view('frontend.about');
    }
    public function profile()
    {
        if(!Auth::check()) {
            return redirect()->action('PageController@register');
        }
        $data = Auth::user();
        return view('frontend.profile',compact('data'));
    }
    public function publication()
    {
        return view('frontend.publication');
    }
    public function runners()
    {
        $runners = Resource::where('category_id',2)->get();
        return view('frontend.runners',compact('runners'));
    }
    public function contacts()
    {
        return view('frontend.contacts');
    }
    public function interesting()
    {
        return view('frontend.interesting');
    }
    public function test()
    {
        $lang = app()->getLocale();
        $data = Pages::where('lang',$lang)->first();
        return view('frontend.test',compact('data'));
    }
    public function login()
    {
        return view('frontend.login');
    }
}
