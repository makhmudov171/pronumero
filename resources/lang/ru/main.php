<?php

return [
    'nav' => [1 => 'О забеге', 2 => 'Участникам', 3 => 'Программа', 4 => 'Кто бежит' , 5 => 'Итоги' , 6 => 'Контакты'],
    'title' => 'Забег <br/> По Самарканду',
    'bottom' => 'Давай с нами, <br/>   будет круто!',
    'register' => 'Регистрация',
    'register_now' => 'Зарегистрируйся сейчас!',
    'profile' => ['cabinet'=>'Мой кабинет'],
    'part'=>'Прими участие в полумарафоне прямо сейчас!',
    'save'=>'Сохранить',
    'content' => [
        'title' => 'Осталось до забега',
        'runners' => 'С нами бегут',
        'contacts' => 'Контакты',
        'organizers' => 'Организаторы',
        'general' => 'Генеральные партнеры',
        'fitness' => 'Партнеры',
        'sponsor' => 'Cпонсоры',
        'interesting' => 'Интересное',
        'all' => 'Все участники',
        'material' => 'Все материалы',
        'partners' => 'Партнеры',
        'social' => 'Следи за событиями:',
        'also' => 'Читайте также',
    ],
    'footer' => [
        'fond' =>
               'Фонд развития культуры и искусства при Министерстве культуры Республики Узбекистан. <br>
                Адрес: 100029  ул. Тараса Шевченко, дом 1.<br>
                Рег. № 552081 <br>
                ИНН: 305122818 ',
        'mail' => 'ЭЛЕКТРОННАЯ ПОЧТА',
        'phone' => 'ТЕЛЕФОНЫ <br> 9:00-18:00 по будним дням <br>',
        'social' => ' СЛЕДИ ЗА СОБЫТИЯМИ <br> В НАШИХ СОЦИАЛЬНЫХ СЕТЯХ',
    ],
    'registration' => [
        '1' => 'Моя анкета',
        '2' => 'Оплата',
        '3' => 'Подтверждение',
        'email' => 'Этот E-mail уже используется',
        'phone' => 'Мобильный телефон',
        'phoneror' => 'Этот номер уже используется',
        'name' => 'Имя',
        'lastname' => 'Фамилия',
        'birthday' => 'Дата рождения',
        'gender' => 'Пол',
        'male' => 'Муж',
        'female' => 'Жен',
        'wrong' => 'Поле заполнено неправильно',
        'country' => 'Страна',
        'city' => 'Город',
        'extra' => 'Экстренный контакт',
        'info' => 'Будьте внимательны при заполнении! Указанный вами номер будет использоваться, чтобы связаться с этим человеком, если с вами что-то случится.',
        'size' => 'Размер футболки',
        'promo' => 'Промо-код',
        'password' => 'Пароль',
        'warn' => 'Будьте внимательны при заполнении! Указанный вами пароль будет использоваться, для входа в профиль.',
        'distance' => 'Выберите дистанцию',
        'sum' => 'сум',
        'time' => 'Планируемое время',
        'a1' => 'Кластер A - быстрее 50 минут',
        'b1' => 'Кластер B - от 50 до 60 минут',
        'c1' => 'Кластер C - дольше 60 минут и бегу первый раз',
        'a2' => 'Кластер A - быстрее 1 часа 40 минут',
        'b2' => 'Кластер B - от 1 часа 40 минут до 2 часов',
        'c2' => 'Кластер C - дольше 2 часов и бегу первый раз, не знаю своё время прохождения дистанции',
        'invalid' => 'Инвалидность',
        'check' => 'Отметьте, если у вас есть официально подтвержденная инвалидность и вы планируете бежать',
        'agreement' =>
            '
                Я осознаю, что в результате участия могут наступить неблагоприятные последствия для здоровья. Я подтверждаю, что я нахожусь в хорошей физической форме и достаточно подготовлен для участия в данном соревновании и мое физическое состояние было подтверждено лицензированным врачом.
                    <br><br>
                При несчастном случае или получении любого физического ущерба, которые могут возникнуть во время моего участия в забеге и после него, я добровольно отказываюсь от каких-либо материальных и иных компенсаций и требований к агентам, сотрудникам, должностным лицам, директорам, преемникам, правопреемникам, спонсорам, поставщикам и иным представителям забега «Samarkand half marathon», который состоится 3 ноября 2019 года. Данный отказ от претензий распространяется на меня, моих родственников, исполнителей, личных представителей, преемников и правопреемников.
                    <br><br>
                Настоящим подтверждением я даю полное разрешение представителям оргкомитета, партнерам и спонсорам данного мероприятия, использовать свое изображение, полученное во время проведения забега в формате фото и видео, для любых законных целей, включая коммерческую рекламу.            
            ',
        'agree1' => 'Я прочитал и согласен с',
        'agree2' => 'условиями предоставления услуг',
        'save' => 'Зарегистрироваться',
        'pay' => 'Мой стартовый взнос',
        'method' => 'Любые способы оплаты',
        'about' => 'О мероприятии',
        'distances' => 'Дистанция',
        'start' => 'Дата старта',
        'place' => 'Место',
        'times' => 'Время',
    ],
    'contacts' => [
        '1' => 'Для участия в качестве волонтеров',
        '2' => '(с темой письма “волонтер”)',
        '3' => 'По тех. вопросам, связанных с регистрацией и оплатой участия',
        '4' => '(с темой письма “регистрация”)',
        '5' => 'По вопросам аккредитации СМИ',
        '6' => 'Заявка на акредитацию',
        '7' => 'По вопросам партнерства:',
        '8' => '(с темой письма «партнерство»)',
        '9' => 'По вопросам образовательного компонента:',
        '10' => '(с темой письма “регистрация”)',
    ],
    'about' => [
        '1' => 'Организатор забега',
        '2' => 'Фонд развития культуры и искусства при Министерстве культуры Республики Узбекистан уже два года реализует проекты в области развития искусства, образования, материально-технического содействия учреждениям культуры и развития меценатства.',
        '3' => 'Подробнее с деятельностью фонда можно ознакомиться на сайте',
        '4' => 'Первый международный благотворительный забег в Узбекистане',
        '5' => 'Samarkand Half Marathon пройдет в городе Самарканде с 1 по 3 ноября 2019 года. Главная цель проекта – привлечь внимание общественности к проблеме инклюзивности на объектах культуры и искусства, а также популяризация здорового образа жизни и развитие спортивного туризма в Узбекистане.',
        '6' => 'В рамках полумарафона пройдет культурная программа, состоящая из образовательных мероприятий, посвященных теме инклюзивности. Среди них будут организованы конференции, лекции и мастер-классы для детей и взрослых. Кроме того, запланированы показы документальных фильмов о спорте и концерты с участием локальных и международных артистов. Подробная программа будет объявлена в сентябре 2019 года.',
        '7' => 'Средства, вырученные с полумарафона, будут направлены на реализацию экспериментального проекта по созданию доступной среды для слабовидящих и незрячих людей в одном из театров Ташкента. Эта программа подразумевает постановку на сцене театра пьесы, с использованием тифлокомментирования. Для этого будет приобретено необходимое оборудование, а также организовано обучение специалистов.',
        '8' => 'В ходе подготовки к полумарафону Фонд развития культуры и искусства проведет исследование, посвященное проблеме инклюзивности – будут проведены тренинги и мониторинги в театрах, музеях и библиотеках Узбекистана. Результаты исследования будут представлены в виде доклада в рамках образовательной программы полумарафона.',
    ],
    'profiles' => [
        'main' => 'Общая информация',
        'personal' => 'Личные данные',
        'change' => 'Смена пароля',
        'old' => 'Старый пароль',
        'new' => 'Новый пароль',
        'confirm' => 'Новый пароль, еще раз',
        'logout' => 'Выйти',
        'photo' => 'Мои фотографии',
        'certificate' => 'Мой сертификат',
        'info' => 'Мои забеги',
    ],
    'forget' => 'Вспомнить пароль',
    'login' => 'Войти',
    'reset' => 'Сбросить пароль',
    'success' => 'Успешно',
    'error' => 'Ошибка',
    'suclog' => 'Успешно вошли в свой кабинет',
    'errlog' => 'Логин или пароль введены неверно',
    'sucpromo' => 'Успешно зарегестрировались по промокоду',
    'change' => 'Успешно изменили ваши данные',
    'logout' => 'Успешно вышли из своего кабинета',
    'information' => [
        '1' => 'Общая информация',
        '2' => 'Регистрация',
        '3' => 'Получение номера',
        '4' => 'В день соревнования',
        '5' => 'Виза и отели',
    ],
    'passed' => [
        '1' => 'Регистрация <br> пройдена!',
        '2' => 'В личном кабинете всегда будет доступна точная информация о забегах, в которых принимается участие. Там же можно изменить свои персональные данные.',
        '3' => 'Личный кабинет',
    ],
    'release' => 'Пресс-релизы',
    'timer' => ['days' => 'Дней','hours' => 'Часов','minute' => 'Минут','second' => 'Секунд'],
];
