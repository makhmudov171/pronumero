@extends('layouts.backend')

@section('content')
<div class="my-3 my-md-5">
    <div class="container-fluid">
    <div class="row row-cards">
        <div class="col-md-6 col-lg-12">
        <div class="card">
            <div class="card-header justify-content-between">
            <h3 class="card-title">
                Страницы
            </h3>
            </div>
            <div class="table-responsive">
<table class="table card-table table-vcenter text-nowrap">
        <thead>
                <tr>
                    <th>Страница</th>
                    <th>Язык</th>
                    <th></th>
                </tr>
        </thead>
        <tbody>
        @foreach($data as $datas)
        <tr>
        <td>
            {{$datas->name}}
        </td>
        <td>
            {{$datas->lang}}
        </td>
        <td>
            <a href="{{action('PanelController@editPage',$datas->id)}}">Изменить</a>
        </td>
        </tr>
        @endforeach
        </tbody>
    </table>
            </div>
        </div>
        </div>
    </div>
</div>
</div>
@endsection
