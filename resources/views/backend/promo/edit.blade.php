@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Params</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('PromoController@update',$id) }}" method="POST" enctype="multipart/form-data">
                        @method('PUT')                        
                        @csrf
                            <div class="form-group">
                                <label for="food-name-uz">Title_uz</label>
                                <input value="{{ $data->title_uz }}" name="title_uz" type="text" class="form-control" id="food-name-uz" placeholder="Enter Food Name">
                            </div>
                            <div class="form-group">
                                <label for="food-name-ru">Title_ru</label>
                                <input value="{{ $data->title_ru }}" name="title_ru" type="text" class="form-control" id="food-name-ru" placeholder="Enter Food Name">
                            </div>
                            <div class="form-group">
                                <label for="startTime">StartTime</label>
                                <input value="{{ $data->start_time }}" name="start_time" type="date" id="startTime" min="2018-01-01">
                            </div>
                            <div class="form-group">
                                <label for="endTime">EndTime</label>
                                <input value="{{ $data->end_time }}" name="end_time" type="date" id="endTime" min="2018-01-01">
                            </div>
                            <label class="custom-switch">
                                <input type="radio" name="slide" value="1" class="custom-switch-input" @if($data->slide == '1') checked="checked" @endif>
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">На Главной</span>
                            </label>
                            <div class="form-group">
                                <input required="required" type="file" class="form-control" name="image">
                            </div>
                        <button class="btn btn-success">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
