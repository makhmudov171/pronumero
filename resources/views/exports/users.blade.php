<table class="table card-table table-vcenter text-nowrap">
        <thead>
                <tr>
                    <th class="w-1">Email</th>
                    <th>Номер телефона</th>
                    <th>Имя</th>
                    <th>Фамилия</th>
                    <th>Дата рождения</th>
                    <th>Пол</th>
                    <th>Страна</th>
                    <th>Город</th>
                    <th>Экстренный контакт</th>
                    <th>Размер футболки</th>
                    <th>Промокод</th>
                    <th>Дистанция</th>
                    <th>Планируемое время</th>
                    <th>Инвалидность</th>
                    <th>Оплата</th>
                    <th>Цена</th>
                    <th>Дата регистрации</th>
                </tr>
        </thead>
        <tbody>
        @foreach($data as $datas)
        @if($datas->id != 1)
        <tr>
        <td>
            {{$datas->email}}
        </td>
        <td>
            {{$datas->phone}}
        </td>
        <td>
            {{$datas->name}}
        </td>
        <td>
            {{$datas->last_name}}
        </td>
        <td>
            {{$datas->birthday}}
        </td>
        <td>
            {{$datas->gender == 0 ? 'Муж': 'Жен'}}
        </td>
        <td>
            {{$datas->country}}
        </td>
        <td>
            {{$datas->city}}
        </td>
        <td>
            {{$datas->extra_phone}}
        </td>
        <td>
            {{$datas->size}}
        </td>
        <td>
            {{$datas->promo}}
        </td>
        <td>
            {{$datas->distance->long}}
        </td>
        <td>
            @if($datas->distance_id == 1)
             @if($datas->level == 'A') Кластер А - быстрее 1 часа 40 минут @endif
             @if($datas->level == 'B') Кластер В - от 1 часа 40 минут до 2 часов @endif
             @if($datas->level == 'C') Кластер С - дольше 2 часов и бегу первый раз, не знаю своё время прохождения дистанции @endif
            @endif
            @if($datas->distance_id == 2)
             @if($datas->level == 'A') Кластер А - быстрее 50 минут @endif
             @if($datas->level == 'B') Кластер В - от 50 до 60 минут @endif
             @if($datas->level == 'C') Кластер С - дольше 60 минут и бегу первый раз, не знаю своё время прохождения дистанции @endif
            @endif
            @if($datas->distance_id == 3)
                Нет
            @endif
        </td>
        <td>
            {{$datas->invalid ? 'Есть' : ' '}}
        </td>
        <td>
            {{$datas->is_paid ? 'Оплатил' : 'Не олатил'}}
        </td>
        <td>
            {{$datas->distance->price.' сум'}}
        </td>
        <td>
            {{$datas->created_at->isoFormat('D MMMM HH:mm')}}
        </td>
        </tr>
        @endif
        @endforeach
        </tbody>
    </table>
