@extends('layouts.frontend')
@section('content')
<div class="contacts__title">
        @lang('main.nav.1')
</div>
<div class="about--info">
    <div class="container">
        <div class="col-md-3">
            <div class="about--info-left">
                <h3>@lang('main.about.1')</h3>
                <p>
                @lang('main.about.2')
                    <br><br>
                @lang('main.about.3')
                        <a href="https://acdf.uz/" target="_blank">www.acdf.uz</a>
                    <br><br>
                        <a href="{{asset('pressrelease_'.app()->getLocale().'.pdf')}}" target="_blank">@lang('main.release')</a>
                </p>
            </div>
        </div>
        <div class="col-md-8 col-md-push-1">
            <div class="about--info-left">
                <h3>@lang('main.about.4')</h3>
<p>
@lang('main.about.5')
<br><br>
@lang('main.about.6')
<br><br>
@lang('main.about.7')
<br><br>
@lang('main.about.8')
</p>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@endsection
