@extends('layouts.frontend')
@section('content')

<section class="section">
         <div class="container">
            <div class="section_left">
                <div class="section_left-title">
                        Делаем сайты идущие в ногу
                        со временем
                </div>
                <div class="section_left-subtitle">
                        Полный спектр услуг по разработке и
                        продвижению эксклюзивных сайтов.
                </div>
                <div class="section_left-btn">
                    <a href="">Посмотреть работы</a>
                </div>
            </div>
        </div>
     </section>


         <div class="prices">
                <div class="prices_title">
                    Наши прайсы
                </div>
                <div class="prices_block">               
                    <div class="prices_block-1">
                        <div class="prices_block-1-img"> <img src="img/target.svg" alt=""> </div>
                        <div class="prices_block-1-h3">Landing page</div>
                        <div class="prices_block-1-h4">от 6 880 000 сум</div>
                        <div class="prices_block-1-p">
                            Лучший выбор для рекламы одного товара когда нужен быстрый результат. Залог успеха – концентрация усилий в одной точке и целенаправленная работа с аудиторией.
                        </div>
                        <a href="">Подробнее</a>
                    </div>
                    <div class="prices_block-1">
                            <div class="prices_block-1-img"> <img src="img/corp.svg" alt=""> </div>
                            <div class="prices_block-1-h3">Корпоративный</div>
                            <div class="prices_block-1-h4">от 11 990 000 сум</div>
                            <div class="prices_block-1-p">
                                Лучший выбор для рекламы одного товара когда нужен быстрый результат. Залог успеха – концентрация усилий в одной точке и целенаправленная работа с аудиторией.
                            </div>
                            <a href="">Подробнее</a>
                        </div>
                        <div class="prices_block-1">
                                <div class="prices_block-1-img"> <img src="img/Cart.svg" alt=""> </div>
                                <div class="prices_block-1-h3">Интернет-магазин</div>
                                <div class="prices_block-1-h4">от 14 990 000 сум</div>
                                <div class="prices_block-1-p">
                                    Лучший выбор для рекламы одного товара когда нужен быстрый результат. Залог успеха – концентрация усилий в одной точке и целенаправленная работа с аудиторией.
                                </div>
                                <a href="">Подробнее</a>
                            </div>
                            <div class="prices_block-1">
                                    <div class="prices_block-1-img"> <img src="img/crown.svg" alt=""> </div>
                                    <div class="prices_block-1-h3">EXCLUSIVE</div>
                                    <div class="prices_block-1-h4">от 31 990 000 сум</div>
                                    <div class="prices_block-1-p">
                                        Лучший выбор для рекламы одного товара когда нужен быстрый результат. Залог успеха – концентрация усилий в одной точке и целенаправленная работа с аудиторией.
                                    </div>
                                    <a href="">Подробнее</a>
                                </div>
                </div>
            </div>

            <div class="about">
                <div class="about_title">
                    О компании
                </div>
                <div class="about_text">
                    Компания Numéro – профессиональная команда, в которую входят
                    web–дизайнеры, программисты, разработчики мобильных
                    приложений, 2D и 3D иллюстраторы, копирайтеры и редакторы.
                    Мы создаём удобные для заказчика и пользователей web-сайты
                    любой сложности, оптимизированные для продвижения в Интернете.
                    Мы разрабатываем полнофункциональные мобильные приложения
                    для iPhone, iPad и Android.
                </div>
            </div>
        <div class="queston">
            <div class="queston_title">
                Чем мы занимаемся?
            </div>
            <div class="queston_text">
                Компания Numéro разрабатывает сайты, порталы и мобильные
                приложения на основе современных стандартов и технологий, таких
                как HTML 5; CSS 3; Python; Laravel; JavaScript; PHP и Java. Работа
                включает весь цикл: от составления технического задания
                до размещения мобильного приложения в магазинах AppStore,
                GooglePlay и Marketplace.
            </div>
        </div>   
        
        <div class="reviews">
            <div class="reviews_title">
                Отзывы наших клиентов
            </div>
            <div class="reviews_subtitle">
                Шахриер (Клиника Кристалл):
            </div>
            <div class="reviews_text">
                Выражаем огромную благодарность команде "NUMERO
                TECHNOLOGIES" в частности Самандару
                Мирахмедову за создание нашего сайта, за терпение и трепетное
                отношение к своему делу!
                Желаем процветания Вашей компании и творческих успехов!
            </div>
        </div>

        <div class="lorem">
            <div class="lorem_title">
                Оставить заявку
            </div>
            <div class="lorem_subtitle">
                Lorem ipsum dolor sit amet
            </div>
            <form action="" class="lorem_form">
                <div class="lorem_form-input">
                    <input type="text" placeholder="Фамилия и Имя">
                    <span class="lorem_form-input-span"> <input type="text" placeholder="Номер телефона"> </span>
                    <input type="text" placeholder="Email">
                </div>
                <div class="lorem_form-text">
                    <textarea name="" id="" cols="30" rows="10" placeholder="Краткое описание проекта и информация"></textarea>
                </div>
                <div class="lorem_text">
                    Нажимая на кнопку, вы подтверждаете согласие на обработку персональных данных.
                </div>
                <a href="">Оставить заявку</a>
            </form>
        </div>

        <div class="trust">
            <div class="trust_title">
                Нам доверяют
            </div>
            <div class="trust_link">
                <div class="trust_link-p1">www.google.com</div>
                <div class="trust_link-p2">www.tatil.uz</div>
                <div class="trust_link-p3">www.evo.uz</div>
                <div class="trust_link-p4">www.google.com</div>
            </div>
        </div>
@endsection