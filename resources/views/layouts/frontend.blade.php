<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="Content-Language" content="{{ app()->getLocale() }}" />
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta property="og:title" content="Samarkand Half Marathon">
    <meta property="og:description" content="Samarkand Half Marathon">
    <meta property="og:image" content="{{asset('img/meta.jpg')}}">
    <meta property="og:url" content="https://samarkandhalfmarathon.uz">
    <meta name="title" content="Самаркандский Полумарафон"/>
    <meta name="description" content="Первый международный благотворительный забег в Узбекистане. Uzbekistan's First International Charity Run"/>
    <title>Samarkand Half Marathon</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,700,700i,900,900i&display=swap&subset=cyrillic" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>
    <body>
        <div class="wrapper">
            @include('partials.nav')
            @yield('content')
            @include('partials.footer')
            </div>
        </div>
    </body>
</html>
