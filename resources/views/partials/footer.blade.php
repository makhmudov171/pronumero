<footer class="footer">
<div class="footer_info">
    <div class="footer_info-logo">
        <a href=""> <img src="img/footer_logo.svg" alt=""> </a>
    </div>
    <div class="footer_info-number">
        +998(90)123-45-67
    </div>
</div>
<div class="footer_fin">
    <div class="footer_fin-adress">
            Проспект Бунёдкор 6А, 100027,
            Ташкент, Узбекистан
    </div>
    <div class="footer_fin-menu">
        <a href="">О компании</a>
        <a href="">Портфолио</a>
        <a href="">Услуги</a>
        <a href="">Контакты</a>
        <a href="">Faq</a>
    </div>
    <div class="footer_fin-social">
        <a href=""> <img src="img/instagram-logo.svg" alt=""> </a>
        <a href=""> <img src="img/facebook-logo.svg" alt=""> </a>
        <a href=""> <img src="img/gmail-logo.svg" alt=""> </a>
        <a href=""> <img src="img/telegram-logo.svg " alt=""> </a>
    </div>
</div>
<div class="footer_p">
            2018 “Pronumero” - все права защищены
    </div>
</footer>